#include "User.h"
#include "RecievedMessage.h"
#include "Helper.h"

User::User(string username, SOCKET sock)
{
	this->_username = username;
	this->_socket = sock;
}

bool User::creatRoom(int roomId, string roomName, int maxUsers, int questionsNo, int questionTime)
{
	
	if (this->_currRoom != nullptr)
	{
		send("1141");
		return false;
	}
	else
	{
		this->_currRoom = new Room(roomId, nullptr, roomName, maxUsers, questionsNo, questionTime);
		send("1140");
		return true;
	}
}

void User::send(string msg)
{
	Helper::sendData(this->_socket, msg);
}

bool User::joinRoom(Room* room)
{
	if (this->_currRoom != nullptr)
	{
		return false;
	}
	else
	{
		room->joinRoom(this);
		return true;
	}
}

void User::leaveRoom()
{
	if (this->_currRoom != nullptr)
	{
		this->_currRoom->leaveRoom(this);
		this->_currRoom = nullptr;
	}
}

int User::closeRoom()
{
	int roomId = this->getRoom()->getid();
	if (this->_currRoom == nullptr)
	{
		return -1;
	}
	else
	{
		if (this->_currRoom->closeRoom(this) != -1)
		{
			delete this->_currRoom;
			this->_currRoom = nullptr;
			return roomId;
		}
		return -1;
	}
}

void User::clearRoom()
{
	this->_currRoom = nullptr;
}

string User::getUsername()
{
	return this->_username;
}

SOCKET User::getSocket()
{
	return this->_socket;
}

Room* User::getRoom()
{
	return this->_currRoom;
}

Game* User::getGame()
{
	return this->_currGame;
}

void User::setGame(Game* game)
{
	this->_currGame = game;
}
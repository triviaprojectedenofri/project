#pragma once
#include <string>
#include <vector>
#include "Question.h"
using namespace std;

class DataBase
{
public:
	DataBase();
	~DataBase();
	bool isUserExists(string);
	bool addNewUser(string, string, string);
	bool isUserAndPassMatch(string, string);
	vector<Question*> initQuestions(int);
	vector<string> getPersonalStatus(string);
	int insertNewGame();
	bool updateGameStatus(int);
	bool addAnswerToPlayer(int, string, int, string, bool, int);

private:
	int static callBackCount(void*, int, char**, char**);
	int static callbackQuestions(void*, int, char**, char**);
	int static callbackBestScores(void*, int, char**, char**);
	int static callbackPersonalStatus(void*, int, char**, char**);

};

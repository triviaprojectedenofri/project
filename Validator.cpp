#include "Validator.h"
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <algorithm>   
using namespace std;

bool Validator::isPasswordValid(string password)
{
	if (password.length() >= 4)
	{
		if (password.find(" ") == string::npos)
		{
			if (isNumber(password))
			{
				if (isUperLetter(password))
				{
					if (isLowerLetter(password))
					{
						return true;
					}
				}
			}
		}
	}
	return false;
}
bool Validator::isUsernameValid(string username)
{
	if (!username.empty())
	{
		if (!isdigit(username[0]))
		{
			if (username.find(" ") == string::npos)
			{
				return true;
			}
		}
	}
	return false;
	
}

bool Validator::isNumber(string s)
{
	return !s.empty() && find_if(s.begin(),s.end(), [](char c) { return !isdigit(c); }) == s.end();
}
bool Validator::isUperLetter(string s)
{ 
	for (size_t i = 0; i < s.length(); i++)
	{
		if (isupper(s[i]) != 0)
		{
			return true;
		}
	}
	return false;
}
bool Validator::isLowerLetter(string s)
{
	for (size_t i = 0; i < s.length(); i++)
	{
		if (islower(s[i]) != 0)
		{
			return true;
		}
	}
	return false;
}
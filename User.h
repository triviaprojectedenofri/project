#pragma once
#include <string>
#include <winsock2.h>
#include <Windows.h>
#include "Game.h"
#include "Room.h"
using namespace std;
class Room;
class User
 {
	public:
		User(string, SOCKET);
		void send(string);
		string getUsername();
		SOCKET getSocket();
		Room* getRoom();
		Game* getGame();
		void setGame(Game*);
		void clearRoom();
		bool creatRoom(int, string, int, int, int);
		bool joinRoom(Room*);
		void leaveRoom();
		int closeRoom();
		bool leaveGame();
		
	private:
		string _username;
		Room* _currRoom;
		Game* _currGame;
		SOCKET _socket;
		};
#include "Room.h"

Room::Room(int id, User* admin, string name,  int maxUsers,  int questionsNo , int questionTime)
{
	this->_id = id;
	this->_admin = admin;
	this->_name = name;
	this->_maxUsers = maxUsers;
	this->_questionsNo = questionsNo;
	this->_questionTime = questionTime;
	this->_users.push_back(admin);
}

bool Room::joinRoom(User* user)
{
	if (this->_maxUsers == user->getRoom()->_users.size())
	{
		send(user->getSocket(), "1101", 4, 0);
		return false;

	}
	else
	{
		this->getUsers().push_back(user);
		send(user->getSocket(), "1100" + this->_questionsNo + this->_questionTime, 8, 0);
		this->sendMessage(this->getUsersListMessage());
		return true;
	}
	send(user->getSocket(), "1102", 4, 0);
	return false;

}

string Room::getUsersListMessage()
{
	if (this == nullptr)
	{
		return "1080";
	}
	string message = ("108" + this->_users.size());
	for (size_t i = 0; i < this->_users.size(); i++)
	{
		message += to_string(this->_users[i]->getUsername().length()) + this->_users[i]->getUsername();
	}
	return message;
}

void Room::sendMessage(string msg)
{
	this->sendMessage(nullptr, msg);
}

void Room::sendMessage(User* user, string msg)
{
	try
	{
		for (size_t i = 0; i < this->_users.size(); i++)
		{
			if (this->_users[i]->getUsername() != user->getUsername())
			{
				this->_users[i]->send(msg);
			}
		}
	}
	catch (const std::exception&)
	{
		//
	}
}

void Room::leaveRoom(User* user)
{
	for (size_t i = 0; i < this->_users.size(); i++)
	{
		if (this->_users[i] == user)
		{
			this->_users.erase(this->_users.begin()+i+1);
			send(user->getSocket(), "1120", 4, 0);
			this->sendMessage(user, this->getUsersListMessage());
		}
	}
}

int Room::closeRoom(User* user)
{
	if (user == this->_admin)
	{
		this->sendMessage("116");
		for (size_t i = 0; i < this->_users.size(); i++)
		{
			if (this->_users[i] != user)
			{
				this->_users[i]->clearRoom();
			}
		}
		return this->getid();
	}
	else
	{
		return -1;
		
	}
}

vector <User*> Room::getUsers()
{
	return this->_users;
}

int Room::getQuestionsNo()
{
	return this->_questionsNo;
}

int Room::getid()
{
	return this->_id;
}

string Room::getName()
{
	return this->_name;
}
﻿#include "TriviaServer.h"
#include <exception>
#include <iostream>
#include <deque>
#include <condition_variable>
#include "Helper.h"
#include "Validator.h"


static const unsigned short PORT = 8826;
static const unsigned int IFACE = 0;
condition_variable cv;
bool ready = false;
enum
{
	SIGN_IN = 200,
	SIGN_OUT = 201,
	ASNWER_SIGN_IN = 102,

};


TriviaServer::TriviaServer()
{
	DataBase *dataBase = new DataBase();
	try
	{
		
		this->_socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	}
	catch (const std::exception& e)
	{
		throw e;
	}
}

TriviaServer::~TriviaServer()
{
	this->_connectedUser.clear();
	this->_roomsList.clear();
	closesocket(this->_socket);
}
void TriviaServer::server()
{
	bindAndListen();
	// create new thread for handling message
	std::thread tr(&TriviaServer::clientHandler,this,this->_socket);
	tr.detach();

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		accept();
	}
}

void TriviaServer::bindAndListen()
{
	struct sockaddr_in sa = { 0 };
	sa.sin_port = htons(PORT);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = IFACE;
	// again stepping out to the global namespace
	if (::bind(_socket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
	{
		throw std::exception("BIND - FAILED");
	}
	if (::listen(_socket, SOMAXCONN) == SOCKET_ERROR)
	{
		throw std::exception("LISTEN - FAILED");
	}
		
}
void TriviaServer::accept()
{
	SOCKET client_socket = ::accept(_socket, NULL, NULL);
	if (client_socket == INVALID_SOCKET)
	{
		throw std::exception("INVALID SOCKET");
	}
	std::thread tr(&TriviaServer::clientHandler,this,client_socket);
	std::unique_lock<std::mutex> lk(this->_mtxRecievedMessages);
	cv.wait(lk, [] {return ready; });
}

void TriviaServer::clientHandler(SOCKET client_socket)
{
	int messgeCode = Helper::getMessageTypeCode(client_socket);
	try
	{
		while (messgeCode != SIGN_OUT || messgeCode != 0)
		{
			addRecieveMessage(buildRecieveMessage(client_socket, messgeCode));
			messgeCode = Helper::getMessageTypeCode(client_socket);
		}
		addRecieveMessage(buildRecieveMessage(client_socket, SIGN_OUT));
	}
	catch (string e)
	{
		addRecieveMessage(buildRecieveMessage(client_socket, SIGN_OUT));
		closesocket(client_socket);
	}
	
}
User* TriviaServer::handleSignin(RecievedMessage* msg)
{
	int amountName = stoi(msg->getValues()[3] + "0") + stoi(msg->getValues()[4]);
	string username;
	string password;
	DataBase database;
	int i = 0;
	for (i = 5; i < 5+amountName; i++)
	{
		username += msg->getValues()[i];
	}
	int amountPass = stoi(msg->getValues()[i] + "0") + stoi(msg->getValues()[i+1]);
	for (int j = i+2; j < j+amountPass; j++ )
	{
		password += msg->getValues()[j];
	}
	if (database.isUserAndPassMatch(username, password))
	{
		if (getUserByName(username) != nullptr)
		{
			send(msg->getSock(),"1022", 4, 0);
		}
		else
		{
			User *newUser = new User(username, msg->getSock());
			this->_connectedUser[msg->getSock()] = newUser;
			send(msg->getSock(), "1020", 4, 0); 
			return newUser;
		}
	}
	else
	{
		send(msg->getSock(), "1021", 4, 0);
	}
	return nullptr;
}
User* TriviaServer::getUserByName(string username)
{
	RecievedMessage *current = this->_queRcvMessages.front();
	while (current != this->_queRcvMessages.back())
	{
		if (current->getUser()->getUsername() == username)
		{
			return current->getUser();
		}
	}
	return nullptr;
}

bool TriviaServer::handleSignup(RecievedMessage* msg)
{
	int amountName = stoi(msg->getValues()[3] + "0") + stoi(msg->getValues()[4]);
	string username;
	string password;
	string email;
	DataBase database;
	int i = 0;
	int j = 0;
	for (i = 5; i < 5 + amountName; i++)
	{
		username += msg->getValues()[i];
	}
	int amountPass = stoi(msg->getValues()[i] + "0") + stoi(msg->getValues()[i + 1]);
	for (int j = i + 2; j < j + amountPass; j++)
	{
		password += msg->getValues()[j];
	}
	int amountEmail = stoi(msg->getValues()[j] + "0") + stoi(msg->getValues()[j + 1]);
	for (i = j + 2; i < j + amountEmail; i++)
	{
		email += msg->getValues()[i];
	}
	if (Validator::isPasswordValid(password))
	{
		if (Validator::isUsernameValid(username))
		{
			if (database.isUserExists(username))
			{
				send(msg->getSock(), "1042", 4, 0); 
				return false;
			}
			else
			{
				if (database.addNewUser(username, password, email))
				{
					send(msg->getSock(), "1040", 4, 0); 
					return true;
				}
				else
				{
					send(msg->getSock(), "1044", 4, 0); 
					return false;
				}
			}
		}
		else
		{
			send(msg->getSock(), "1043", 4, 0);
			return false;
		}
	}
	else
	{
		send(msg->getSock(), "1041", 4, 0); 
		return false;
	}
}

bool TriviaServer::handleCreateRoom(RecievedMessage* msg)
{
	string roomName;
	int playersNumber, questionsNumber, questionTimeInSec, i;
	if (msg->getUser() != nullptr)
	{
		int amountRoomName = stoi(msg->getValues()[3] + "0") + stoi(msg->getValues()[4]);
		for (i = 5; i < 5 + amountRoomName; i++)
		{
			roomName += msg->getValues()[i];
		}
		playersNumber = stoi(msg->getValues()[i + 1]);
		questionsNumber = stoi(msg->getValues()[i + 2] + "0") + stoi(msg->getValues()[i+3]);
		questionTimeInSec = stoi(msg->getValues()[i + 4] + "0") + stoi(msg->getValues()[i + 5]);
		this->static_roomSequence += 1;
		if (msg->getUser()->creatRoom(this->static_roomSequence, roomName, playersNumber, questionsNumber, questionTimeInSec))
		{
			this->_roomsList[this->static_roomSequence] = msg->getUser()->getRoom();
			return true;
		}
	}
	return false;
}

bool TriviaServer::handleJoinRoom(RecievedMessage* msg)
{
	User *user = msg->getUser();
	if (user == nullptr)
	{
		return false;
	}
	string stringId = msg->getValues()[3] + msg->getValues()[4] + msg->getValues()[5] + msg->getValues()[6];
	int roomId = stoi(stringId);
	Room *room = getRoomById(roomId);
	if (room == nullptr)
	{
		send(user->getSocket(), "1102", 4, 0);
		return false;
	}
	else
	{
		user->joinRoom(room);
		return true;
	}
	
}

Room* TriviaServer::getRoomById(int roomId)
{
	for (size_t i = 0; i < this->_roomsList.size(); i++)
	{
		if (this->_roomsList[i]->getid() == roomId)
		{
			return this->_roomsList[i];
		}
	}
	return nullptr;
}

void TriviaServer::handleGetRooms(RecievedMessage* msg)
{
	string message = "106" + to_string(this->_roomsList.size());
	for (size_t i = 0; i < this->_roomsList.size(); i++)
	{
		message += to_string(this->_roomsList[i]->getid() + this->_roomsList[i]->getName().length()) + this->_roomsList[i]->getName();
	}
	send(msg->getUser()->getSocket(), message.c_str(), message.length(), 0);
}

bool TriviaServer::handleLeaveRoom(RecievedMessage* msg)
{
	User* user = msg->getUser();
	if (user == nullptr)
	{
		return false;
	}
	if (user->getRoom() == nullptr)
	{
		return false;
	}
	else
	{
		user->leaveRoom();
		return true;
	}
}

bool TriviaServer::handleCloseRoom(RecievedMessage* msg)
{
	Room *room = msg->getUser()->getRoom();
	std::map<int, Room*>::iterator it;
	if (room == nullptr)
	{
		return false;
	}
	else
	{
		if (msg->getUser()->closeRoom() != -1)
		{
			it = this->_roomsList.find(msg->getUser()->getRoom()->getid());
			this->_roomsList.erase(it);
			return true;
		}
		return false;
	}
}

void TriviaServer::handleGetUsersInRoom(RecievedMessage* msg)
{
	User *user = msg->getUser();
	int roomId = stoi(msg->getValues()[3] + msg->getValues()[4] + msg->getValues()[5] + msg->getValues()[6]);
	Room *room = this->getRoomById(roomId);
	if (room == nullptr)
	{
		send(msg->getUser()->getSocket(), "1080", 4, 0);
	}
	else
	{
		 string message = msg->getUser()->getRoom()->getUsersListMessage();
		 send(msg->getUser()->getSocket(), message.c_str(), message.length(), 0);
	}
}

void TriviaServer::addRecieveMessage(RecievedMessage* msg)
{
	std::lock_guard<std::mutex> lock(this->_mtxRecievedMessages);
	this->_queRcvMessages.push(msg);
	if (returnBoolValue())
	{
		cv.notify_one();
	}
}
RecievedMessage* TriviaServer::buildRecieveMessage(SOCKET sc, int msgCode)//problem
{
	string message = Helper::getStringPartFromSocket(sc, 2000);
	
	vector <string> messageVector;
	if (message != "")
	{
		messageVector.push_back(message);
		RecievedMessage rec = RecievedMessage(sc, msgCode, messageVector);
		return &rec;
	}
	return nullptr;
}


bool TriviaServer::returnBoolValue()
{
	return true;
}
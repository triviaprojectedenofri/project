#include "DataBase.h"
#include <fstream>
#include <iostream>
using namespace std;

std::fstream users;
DataBase::DataBase()
{
	try
	{
		users.open("Users.txt", std::fstream::in | std::fstream::out);
	}
	catch (string e)
	{
		throw e;
	}
	
}
DataBase::~DataBase()
{
	users.close();
}


bool DataBase::isUserAndPassMatch(string username, string password)
{
	ifstream users("Users.txt");
	std::string str;
	while (getline(users, str))
	{
		if (str == (username + ":" + password))
		{
			return true;
		}
	}
	return false;
}

bool DataBase::isUserExists(string username)
{
	if (!users) 
	{
		cout << "Cannot open input file.\n";
		return false;
	}
	std::string line;
	while (getline(users, line)) 
	{
		size_t location = line.find(":");
		if (line.find(username) < location)
		{
			return true;
		}
	}
	return false;
}

bool DataBase::addNewUser(string username, string password, string email)
{
	try
	{
		ofstream myfile("Users.txt");
		myfile << username+":"+password+":"+email+"\n";
		myfile.close();	
	}
	catch (const std::exception&)
	{
		return false;
	}
	return true;
}
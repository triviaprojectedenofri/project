#pragma once
//#include <iostream>
//#include <thread>
#include <WinSock2.h>
#include <Windows.h>
#include <map>
#include <mutex>
#include <queue>
#include <string>
#include "DataBase.h"
#include "RecievedMessage.h"
#include "Room.h"
#include"User.h"

using namespace std;

class TriviaServer
{
public:
	TriviaServer();
	~TriviaServer();
	void server();

private:
	SOCKET _socket;
	map<SOCKET, User*> _connectedUser;
	DataBase _db;
	map<int, Room*> _roomsList;
	mutex _mtxRecievedMessages;
	queue<RecievedMessage*> _queRcvMessages;
	int static_roomSequence;

	void bindAndListen();
	void accept();
	void clientHandler(SOCKET);
	void safeDeleteUser(RecievedMessage*);

	User* handleSignin(RecievedMessage*);
	bool handleSignup(RecievedMessage*);
	void handleSignout(RecievedMessage*);

	void handleLeaveGame(RecievedMessage*);
	void handleStartGame(RecievedMessage*);
	void handlePlayerAnswer(RecievedMessage*);

	bool handleCreateRoom(RecievedMessage*);
	bool handleCloseRoom(RecievedMessage*);
	bool handleJoinRoom(RecievedMessage*);
	bool handleLeaveRoom(RecievedMessage*);
	void handleGetUsersInRoom(RecievedMessage*);
	void handleGetRooms(RecievedMessage*);

	void handleGetBestScores(RecievedMessage*);
	void handleGetPersonalStatus(RecievedMessage*);

	void handleRecievedMessages();
	void addRecieveMessage(RecievedMessage*);
	RecievedMessage* buildRecieveMessage(SOCKET, int);

	User* getUserByName(string);
	User* getUserBySocket(SOCKET);
	Room* getRoomById(int);
	bool returnBoolValue();







	
	

};

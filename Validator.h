#pragma once
#include <string>
using namespace std;

static class Validator
{
public:
	bool static isPasswordValid(string);
	bool static isUsernameValid(string);

private:
	bool static isNumber(string);
	bool static isUperLetter(string);
	bool static isLowerLetter(string);

} Validator;


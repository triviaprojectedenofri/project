#pragma once
#include <vector>
#include <map>
#include "User.h"
#include "DataBase.h"

using namespace std;
class User;
class Game
 {
	public:
		Game(const vector<User*>&, int, DataBase&);
		~Game();
		void sendFirstQuestion();
		void handleFinishGame();
		bool handleNextTurn();
		bool handleAnswerFromUser(User*, int, int);
		bool leaveGame(User*);
		int getID();
		
	private:
		vector<Question*> _question;
		vector<User*> _players;
		int _questions_no;
		int currQuestionlndex;
		DataBase& _db;
		map <string, int> _results;
		int _currentTurnAnswers;
				
			bool insertGameToDB();
		void initQuestionsFormDB();
		void sendQuestionToAllUsers();
		};